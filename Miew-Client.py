from flask import Flask, request
from threading import Thread
import requests
import logging
import os
from color import *


log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


# Partie permettant la récupération des infos qui sont envoyées au serveur
class ClientAPI(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        app = Flask(__name__)
        app.debug = False
        app.config['SECRET_KEY'] = 'secret!'

        @app.route('/rcv/', methods=['POST'])
        def rcv():
            message = request.form.get('message')
            pseudo = request.form.get('name')
            to_display = color.GREEN + "<" + pseudo + ">" + color.END + message
            print(to_display)
            return pseudo

        if __name__ == '__main__':
            app.run(host='0.0.0.0', port=8001)


# Le client en lui même
class ClientCli(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        server = input('Serveur : ')
        server += ':8000'
        login = input('Quel est ton pseudo ? ')
        requests.post("http://" + server + "/login/", data={'name': login})

        while True:
            message = input('Message : ')
            # Gestion de la déconnexion
            if message == "/quit":
                requests.post("http://" + server + "/logout/")
                os._exit(0)
            # Gestion du changement de pseudo
            if message.startswith('/pseudo'):
                requests.post("http://" + server + "/pseudo/", data={'name': message.split(' ')[1]})
                continue
            elif message.startswith('/people'):
                requests.post("http://" + server + "/people/")
                continue
            requests.post("http://" + server + "/send/", data={'message': message})



# Creation des threads
thread_1 = ClientAPI()
thread_2 = ClientCli()

# Lancement des threads
thread_1.start()
thread_2.start()

# Attend que les threads se terminent
thread_1.join()
thread_2.join()