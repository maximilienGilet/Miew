from flask import Flask, request
import requests
from color import *

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'


annuaire = {}
socks = {}


@app.route('/')
def hello_world():
    return 'Hello Miew !'


@app.route('/send/', methods=['POST'])
def send_route():
    message = request.form.get('message')
    pseudo = get_pseudo(request.remote_addr)
    dispatch(message, pseudo)
    return message


@app.route('/login/', methods=['POST'])
def login():
    name = request.form.get('name')
    ip = request.remote_addr
    annuaire[ip] = name
    dispatch(name + ' vient de se connecter', '', True)
    nb_utils = len(annuaire)
    if nb_utils > 1:
        dispatch(str(nb_utils) + ' utilisateurs de connectés actuellement.')
    else:
        dispatch('Tu es tout seul.. Invite des amis à te rejoindre !')
    return name + ' ' + ip


def set_name_to_ip(ip, name):
    annuaire[ip] = name


@app.route('/pseudo/', methods=['POST'])
def change_pseudo():
    pseudo = request.form.get('name')
    ip = request.remote_addr
    vieux_pseudo = annuaire.get(ip)
    set_name_to_ip(ip, pseudo)
    dispatch(vieux_pseudo + " vient de changer son pseudo en " + annuaire.get(ip), "SERVEUR", True)


@app.route('/people/', methods=['POST'])
def people():
    liste = ''
    if len(annuaire) > 1:
        for ip in annuaire:
            liste += get_pseudo(ip) + ', '
        liste = liste[:-2]
        liste += ' sont actuellement connectés au serveur.'
    else:
        liste += 'Personne ne viendra à ton secours, même si tu cries.'
    dispatch(liste, "SERVEUR", True)
    return 'people'


@app.route('/logout/', methods=['POST'])
def logout():
    ip = request.remote_addr
    dispatch(annuaire.get(ip) + " vient de se deconnecter.", "SERVEUR", True)
    del annuaire[ip]
    return 'logout'

# Envoie les messages à tous les clients
def dispatch(message, name='', special=False):
    for ip in annuaire:
        requests.post('http://'+ip+':8001/rcv/', {'name': name, 'message': message})
    if special:
        print(color.BOLD + message + color.END)
    else:
        print(color.GREEN + '<' + name + '> ' + color.END + message)


def get_pseudo(addr):
    return annuaire.get(addr, 'Anon')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
