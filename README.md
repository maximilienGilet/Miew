<p align="center"><img src="MiewLogo.png" height="200" width="200"></p>
<h1 align="center">Miew</h1>
<h3 align="center">Miew Is Exactly What_you_want</h3>

**Miew is a simple Python school project made in 1 day**


## A propos

Miew est un ensemble client - serveur, permettant de dialoguer entre différentes machines via le réseau IP.

Le serveur a été designé pour être utilisé comme une API, interrogable par des clients dédiés.

Une partie client a été réalisée, elle est utilisable en ligne de commande.


## Installation

1. Pour installer Miew, il vous suffit de récupérer ce dépot :

        $ git clone https://maximilienGilet@gitlab.com/maximilienGilet/Miew.git    

2. Installez les dépendances : 

        $ pip3 install flask gevent requests 
        

## Utilisation

**NB: Miew nécessite l'utilisation des ports 8000 et 8001, ces ports doivent être ouverts sur votre machine**

### Serveur

Pour lancer le serveur, utilisez simplement la commande suivante: 

    $ python3 Miew.py
    
### Client

Pour lancer le client, executez la commande suivante :

    $ python3 Miew-Client.py
    
Il vous sera demandé de fournir l'adresse IP du serveur sur lequel vous voulez vous connecter.

#### Liste des commandes utiles

* `/quit` : permet de quitter le client
* `/pseudo {nouveau pseudo}`: permet de changer de pseudo
* `/people` : permet de voir qui est connecté 


## Licence

MIT